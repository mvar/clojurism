(ns series)

(defn slices [string length]
  (if (= length 0)
    [""]
    (let [i 0
          strlen (count string)]
      (loop [pos i
             acc []]
        (if (> (+ pos length) strlen)
          acc
          (recur (inc pos) (conj acc (subs string pos (+ pos length)))))
        )
      )
    )
  )
