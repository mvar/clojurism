(ns armstrong-numbers
  (:require [clojure.math.numeric-tower :refer [expt]])
  )

(defn- arm-sum
  "return armstrong sum for given number"
  ([digits]
   (reduce + (map (fn [x] (expt x (count digits))) digits))))

(defn- number-of-digits
  "returns the number of digits"
  ([number]
   (loop [remaining number
          count ()]
     (let [rest (quot remaining 10)
           digit (rem remaining 10)]
     (if (= rest 0)
       (conj count digit)
       (recur rest (conj count digit)))
       )
     )
   )
  )

(defn armstrong?
  "An Armstrong number is a number that is the sum of its own digits each raised
  to the power of the number of digits."
  [num]
  (if (= (arm-sum (number-of-digits num)) num)
    true
    false
    )
  )
