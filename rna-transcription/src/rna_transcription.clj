(ns rna-transcription)

(defn- get-rna-complement [d]
  (let [rnamap {\G \C
                \C \G
                \T \A
                \A \U}
        c (get rnamap d)]
    (if (nil? c)
      (throw (AssertionError. "Wrong input."))
      c)))

(defn to-rna [dna]
  (->> dna
       (map get-rna-complement)
       (apply str)))
