(ns bob
  (:require [clojure.string :as str]))

(defn- all-caps? [s]
  (and (seq s) (every? #(Character/isUpperCase %) s)))

(defn response-for [s]
  (let [question? (str/ends-with? s "?")
        letters (filter #(Character/isLetter %) s)
        allcaps? (all-caps? letters)]
    (cond
      (str/blank? s)
      "Fine. Be that way!"
      (and question? allcaps?)
      "Calm down, I know what I'm doing!"
      question?
      "Sure."
      allcaps?
      "Whoa, chill out!"
      :else "Whatever.")))
