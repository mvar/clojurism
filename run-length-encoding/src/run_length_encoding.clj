(ns run-length-encoding)

(defn count-letters
  "counts the letters on provided string until a different
  letter is encountered. Returns map of parsed/counted letter
  and remaining string"
  [s]
  (let [current-letter (first s)
        rest-letters (rest s)]
    (loop [acc 1
           cl current-letter
           rl rest-letters]
        (if (not (= cl (first rl)))
          (if (= acc 1)
            {:parsed cl :rest rl}
            {:parsed (str acc cl) :rest rl})
          (recur (+ acc 1) cl (rest rl))))))

(defn run-length-encode
  "encodes a string with run-length-encoding"
  [plain-text]
  (let [t plain-text]
    (loop [et {:parsed "" :rest t}
           rt (:parsed et)]
      (if (empty? (:rest et))
        (str rt (:parsed et))
        (recur (count-letters (:rest et)) (str rt (:parsed et)))))))

    
(defn run-length-decode
  "decodes a run-length-encoded string"
  [cipher-text]
  (loop [ss cipher-text
         result ""]
    (let [[match numletter remaining] (re-find #"(^[0-9]*[a-zA-Z ])(.*)$" ss)]
      (if (empty? match)
        result
        (let [[_ num letter] (re-find #"(^[0-9]*)(.*)$" numletter)
              num (#(if (empty? num)
                      1
                      (Integer/parseInt num)))
              new-result (str result (apply str (repeat num letter)))]
          (recur remaining new-result))))))
