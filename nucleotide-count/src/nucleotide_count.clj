(ns nucleotide-count)

(defn nucleotide-counts
  "returns map with counts of nucleotides (ORLY?)"
  [strand]
  (if (empty? strand)
    {\A 0 \T 0 \G 0 \C 0}
    (let [nm {\A 0 \T 0 \G 0 \C 0}
          str strand]
      (loop [nucmap nm
             s str]
        (let [currentChar (first s)
              restChars (rest s)]
          (if (empty? restChars)
            (update nucmap currentChar inc)
            (recur (update nucmap currentChar inc) restChars)))))))

(defn count
  "counts nucleotide in given strand (ORLY?)"
  [nucleotide strand]
  (if (contains? #{\A \C \G \T} nucleotide)
    (let [nm (nucleotide-counts strand)]
      (second (find nm nucleotide)))
    (throw (Exception. "Invalid nucleotide " nucleotide))))
