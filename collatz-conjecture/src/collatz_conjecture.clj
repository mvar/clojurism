(ns collatz-conjecture)

(defn collatz
  "Take any positive integer n. If n is even, divide n by 2 to get n / 2. If n
  is odd, multiply n by 3 and add 1 to get 3n + 1. Repeat the process
  indefinitely. The conjecture states that no matter which number you start
  with, you will always reach 1 eventually.
  Given a number n, return the number of steps required to reach 1."
  [num]
  (if (< num 1)
    (throw (Throwable. "Please enter a positive number"))
    (count (take-while (fn [r] (if (> r 1)
                                 true
                                 false)) (iterate (fn [x]
                                                    (if (= (rem x 2) 0)
                                                      (/ x 2)
                                                      (+ (* x 3) 1)))
                                                  num)
                       )
           )
    )
  
  )
