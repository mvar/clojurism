(ns reverse-string)

(defn reverse-string
  ([s]
   (loop [remaining s
          news ""]
     (if (empty? remaining)
       news
       (recur (subs remaining 1) (str (first remaining) news))))))

(defn reverse-string2 [s]
  (if (empty? s)
    nil
    (let [sub (subs s 1)
          ch (first s)]
      (str (reverse-string sub) ch))))
