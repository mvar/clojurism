(ns two-fer)

(defn two-fer
  "one for someone one for me"
  ([] (two-fer "you"))
  ([name] (str "One for "name", one for me.")))

